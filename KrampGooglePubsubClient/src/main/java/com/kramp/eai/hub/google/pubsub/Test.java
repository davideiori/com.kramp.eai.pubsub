package com.kramp.eai.hub.google.pubsub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;

public class Test {

	public static void publishMessages() throws Exception {
		// [START pubsub_publish]
		TopicName topicName = TopicName.create("kramp-integraton-tst", "catalogitem");
		Publisher publisher = null;
		List<ApiFuture<String>> messageIdFutures = new ArrayList<>();

		try {

			// Create a publisher instance with default settings bound to the topic
			publisher = Publisher.defaultBuilder(topicName).build();

			List<String> messages = Arrays.asList("second message");

			// schedule publishing one message at a time : messages get automatically
			// batched
			for (String message : messages) {

				ByteString data = ByteString.copyFromUtf8(message);
				PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();
				ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
				ApiFutures.addCallback(messageIdFuture, new ApiFutureCallback<String>() {
					@Override
					public void onSuccess(String messageId) {
						System.out.println("published with message id: " + messageId);
					}

					@Override
					public void onFailure(Throwable t) {
						System.out.println("failed to publish: " + t);
					}
				});

			}
		} finally {
			// wait on any pending publish requests.
			List<String> messageIds = ApiFutures.allAsList(messageIdFutures).get();

			for (String messageId : messageIds) {
				System.out.println("published with message ID: " + messageId);
			}

			if (publisher != null) {
				// When finished with the publisher, shutdown to free up resources.
				publisher.shutdown();
			}
		}
		// [END pubsub_publish]
	}

	public static void main(String... args) throws Exception {

		publishMessages();

	}

}